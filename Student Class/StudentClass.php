<?php



	
	Class Student {
		
		
		var $Student_Id = "19-41803";
		var $FirstName = "Edward";
		var $MiddleName = "Puting";
		var $LastName = "Kanin";
	    var $BirthDate = "02/25/1989";
		var $Address = "Libmana, Camarines Sur.";
		
		 
		
		public function __construct() {
		}	
		public function getStudent_Id() {
			return $this->Student_Id;
		}
		
		
		public function setStudent_Id($_Student_Id) {
			$this->Student_Id = $_Student_Id;
		
		}
		public function getFirstName() {
			return $this->FirstName;
		}
		
		
		public function setFirstName($_FirstName) {
			$this->FirstName = $_FirstName;
		
		}
		public function getMiddleName() {
			return $this->MiddleName;
		}
		
		
		public function setMiddleName($_MiddleName) {
			$this->MiddleName = $_MiddleName;
		
		}
		public function getLastName() {
			return $this->LastName;
		}
		
		
		public function setLastName($_LastName) {
			$this->LastName = $_LastName;
		
		}
		public function getBirthDate() {
			return $this->BirthDate;
		}
		
		
		public function setBirthDate($_BirthDate) {
			$this->BirthDate = $_BirthDate;
		
		}
		public function getAddress() {
			return $this->Address;
		}
		
		
		public function setAddress($_Address) {
			$this->Address = $_Address;
		
		}

		

	}


?>